import { Injectable, Input } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Pokemon } from "../interfaces/pokemon";
import { UserService } from "src/app/services/user.service";

@Injectable()
export class PokemonService {
  constructor(private http: HttpClient) {}

  pokemon: Pokemon;
  user: UserService;

  ngOnInit() {}

  chargerPokemon(id: number): Promise<Pokemon> {
    return this.http
      .get<Pokemon>("https://pokeapi.co/api/v2/pokemon/" + id)
      .toPromise();
  }

  chargerDeck() {
    return this.user.getUserDeck();
  }
}