import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PageLoginComponent } from "./components/page-login/page-login.component";
import { PageBoutiqueComponent } from "./components/page-boutique/page-boutique.component";
import { PageErrorComponent } from "./components/page-error/page-error.component";
import { PageDeckComponent } from "./components/page-deck/page-deck.component";
import { MenuComponent } from "./components/menu/menu.component";

const routes: Routes = [
  { path: "", component: PageLoginComponent },
  {
    path: "",
    component: MenuComponent,
    children: [
      { path: "deck", component: PageDeckComponent },
      { path: "boutique", component: PageBoutiqueComponent },
      { path: "error", component: PageErrorComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}