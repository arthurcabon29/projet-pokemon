import { Component, OnInit } from "@angular/core";
import { PokemonService } from "../../services/pokemon.service";
import { UserService } from "../../services/user.service";
import { Pokemon } from "../../interfaces/pokemon";
import { User } from "src/app/interfaces/user";
import { MenuComponent } from "../menu/menu.component";

@Component({
  selector: "app-page-deck",
  templateUrl: "./page-deck.component.html",
  styleUrls: ["./page-deck.component.scss"]
})
export class PageDeckComponent implements OnInit {
  deck: Array<Pokemon> = [];
  emptyDeck: boolean = true;
  myUser: User;
  minAttack: number;

  constructor(private pokemon: PokemonService, private user: UserService, private menu: MenuComponent) {}

  async ngOnInit() {
    this.minAttack = 0;
    this.showDeck();
    this.menu.loadMenuCoins();
  }

  async showDeck() {
    console.log(this.user.getUserDeck());
    console.log(this.user.getUserDeck().length);
    
    
    for (let i = 0; i < this.user.getUserDeck().length; i++) {
      this.emptyDeck = false;
      this.deck.push(await this.pokemon.chargerPokemon(this.user.getUserDeck()[i]));
    }
  }
}